#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define B_IN_MB    (1024 * 1024)

int
main(int argc, char *argv[])
{
    uint8_t *array;
    size_t array_len;
    size_t i = 0;

    if ( argc != 2 ) {
        fprintf(stderr, "usage: ./memory-user <#-MB-to-allocate>\n");
        exit(EXIT_FAILURE);
    }

    array_len = (size_t)atoi(argv[1]) * B_IN_MB;

    array = malloc(array_len);
    if ( array == NULL ) {
        fprintf(stderr, "failed to allocate memory\n");
        exit(EXIT_FAILURE);
    }

    while ( true ) {
        array[(i % array_len)] = i % UINT8_MAX;
        i++;
    }

    return EXIT_SUCCESS;
}
