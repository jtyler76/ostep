#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(void)
{
    pid_t pid = fork();

    if ( pid == 0 ) {
        /* parent */
        wait(NULL);
    } else {
        /* child */
        char * env[] = {
            "VAR1=foo",
            "VAR2=bar",
            NULL,
        };
        execle("/usr/bin/env", "env", NULL, env);
    }

    return EXIT_SUCCESS;
}
