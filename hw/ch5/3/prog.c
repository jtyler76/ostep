#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(void)
{
    pid_t pid = fork();

    if ( pid == 0 ) {
        wait(NULL);
        printf("goodbye\n");
    } else {
        printf("hello\n");
    }

    return EXIT_SUCCESS;
}
