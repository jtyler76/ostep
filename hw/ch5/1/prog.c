#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int
main(void)
{
    int x = 100;
    pid_t pid = fork();

    if ( pid == 0 ) {
        printf("parent :: pid = %d\n", getpid());
        printf("parent :: x-before-change = %d\n", x);
        x = 50;
        printf("parent :: x-after-change = %d\n", x);
    } else {
        printf("child :: pid = %d\n", getpid());
        printf("child :: parent = %d\n", pid);
        printf("child :: x-before-change = %d\n", x);
        x = 25;
        printf("child :: x-after-change = %d\n", x);
    }

    return EXIT_SUCCESS;
}
