#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int
main(void)
{
    int fd = open("file.txt", (O_CREAT | O_RDWR), 0644);
    pid_t pid = fork();

    if ( pid == 0 ) {
        printf("parent :: pid = %d\n", getpid());
        printf("parent :: fd = %d\n", fd);
        if ( write(fd, "I'm the parent!", strlen("I'm the parent!")) == -1 ) {
            printf("parent :: write() failed\n");
        }
    } else {
        printf("child :: pid = %d\n", getpid());
        printf("child :: parent = %d\n", pid);
        printf("child :: fd = %d\n", fd);
        if ( write(fd, "I'm the child!", strlen("I'm the child!")) == -1 ) {
            printf("child :: write() failed\n");
        }
    }

    return EXIT_SUCCESS;
}
