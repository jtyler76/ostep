#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(void)
{
    pid_t pid = fork();

    if ( pid == 0 ) {
        /* child */
        printf("child :: pid = %d\n", getpid());
    } else {
        /* parent */
        printf("parent :: pid = %d\n", getpid());
        printf("parent :: child (%d) created\n", pid);
        pid_t chid = waitpid(-1, NULL, 0);
        printf("parent :: child %d died\n", chid);
    }

    return EXIT_SUCCESS;
}
