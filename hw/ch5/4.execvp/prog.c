#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(void)
{
    pid_t pid = fork();

    if ( pid == 0 ) {
        /* child */
        char *args[] = {
            "env",
            NULL,
        };

        char *env[] = {
            "VAR1=foo",
            "VAR2=bar",
            NULL
        };

        execvpe("env", args, env);

    } else {
        /* parent */
        wait(NULL);
    }

    return EXIT_SUCCESS;
}
