#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(void)
{
    pid_t pid = fork();

    if ( pid == 0 ) {
        /* parent */
        wait(NULL);
    } else {
        /* child */
        char *args[] = {
            "ls",
            "-l",
            NULL,
        };
        execv("/bin/ls", args);
    }

    return EXIT_SUCCESS;
}
