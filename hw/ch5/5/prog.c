#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(void)
{
    pid_t pid = fork();

    if ( pid == 0 ) {
        /* child */
        printf("child :: pid = %d\n", getpid());
        pid_t rc_wait = wait(NULL);
        printf("child :: wait(NULL) returned %d (errno = %d, %s)\n",
                rc_wait, errno, strerror(errno));
    } else {
        /* parent */
        printf("parent :: pid = %d\n", getpid());
        printf("parent :: child (%d) created\n", pid);
        pid_t chid = wait(NULL);
        printf("parent :: child %d died\n", chid);
    }

    return EXIT_SUCCESS;
}
