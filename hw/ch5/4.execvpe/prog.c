#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(void)
{
    pid_t pid = fork();

    if ( pid == 0 ) {
        /* child */
        char *args[] = {
            "ls",
            "-l",
            NULL,
        };
        execvp("ls", args);
    } else {
        /* parent */
        wait(NULL);
    }

    return EXIT_SUCCESS;
}
