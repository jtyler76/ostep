#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
    pid_t pid1, pid2;
    int pipe_fds[2] = { 0 };

    /* create a pipe */
    pipe(pipe_fds);

    /* child #1 - runs ls */
    pid1 = fork();
    if ( pid1 == 0 ) {
        /* close the unused read end of the pipe */
        close(pipe_fds[0]);  // (C)

        /* connect stdout of this process to the pipe */
        dup2(pipe_fds[1], STDOUT_FILENO);
        close(pipe_fds[1]);

        execlp("ls", "ls",  NULL);
    }

    /* child #2 - runs wc on it's input */
    pid2 = fork();
    if ( pid2 == 0 ) {
        /* close the unused write end of the pipe */
        close(pipe_fds[1]);  // (D)

        /* connect the stdin of this process to the pipe */
        dup2(pipe_fds[0], STDIN_FILENO);
        close(pipe_fds[0]);

        execlp("wc", "wc", NULL);
    }

    /* The parent doesn't interact with the pipe, so close the file
     * descriptors. */
    close(pipe_fds[0]);  // (A)
    close(pipe_fds[1]);  // (B)

    wait(NULL);
    wait(NULL);

    return EXIT_SUCCESS;
}
