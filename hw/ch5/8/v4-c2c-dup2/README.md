# Child -> Child ~ Pipe Connections with `dup2()`

This example creates a pipe with `pipe()`, and then `fork()`'s two children. One that `exec()`'s to `ls`, and the other that `exec()`'s to `wc`. The `stdout` of the `ls` process is connected to the write end of the pipe, and the `stdin` of the `wc` process is connected to the read end of the pipe.


## Connecting `stdin`/`stdout` to Pipes Using `dup2()`

The `stdin` and `stdout` "files" are described by the `STDIN_FILENO` and `STDOUT_FILENO` fds respectively. The standard library (`libc`) APIs use those fds to write to or read from `stdin` and `stdout`. The default `stdin` and `stdout` (i.e. what `STDIN_FILENO` and `STDOUT_FILENO` point to) should be set up by the shell that you execute the program from.

In order to change the `stdin` and/or `stdout` for a process to the read and/or write ends of a pipe, we need `STDOUT_FILENO` to point to the write end of the pipe, and `STDIN_FILENO` to point to the read end of the pipe. The original way of doing this was with the `dup()` function, but that method is a bit rickitty and isn't guaranteed to work in all situations. The newer and better method is to use `dup2()`.

```c
dup2(pipe_fds[0], STDIN_FILENO);    // make STDIN_FILENO reference the read end of the pipe
close(pipe_fds[0]);                 // close the original fd for the read end of the pipe

// or

dup2(pipe_fds[1], STDOUT_FILENO);    // make STDOUT_FILENO reference the write end of the pipe
close(pipe_fds[1]);                  // close the original fd for the write end of the pipe
```

The `dup2()` calls close the FD from the second argument (`STDIN_FILENO` and `STDOUT_FILENO`), and create a new FD with the same value but that points to the same object references by the first argument (the pipe descriptor). This means that in the above example `STDIN_FILENO` will now reference the *read* end of the pipe, and `STDOUT_FILENO` will now reference the *write* end of the pipe.

At this point you'll have 2 fds pointing to each end of the pipe, the originals can/should be closed.


## Closing Unused Pipe FDs

### The Parent

The **parent** process doesn't use the pipe at all, so as soon as it has `fork()`'d it's children it should close both ends of the pipe (**A** and **B**).

If the *write* end of the pipe isn't closed (**B**), then the reading process will never get an `EOF` because that only occurs when all FDs to the *write* end of the pipe are closed. This means that the `wc` process would never terminate, continually waiting for the `EOF`.

If the *read* end of the pipe isn't closed (**A**), there will be no functional difference for this program. It's still best practice though. Just know that keeping extra open FDs to the *read* end of the pipe can interfere with the signaling mechanism intended to for the reading process to exit when there are no *read* connections to the pipe.


### The `ls` Child

The `ls` **child** is the writing process. It doesn't use the *read* end of the pipe, so it should close it (**C**). In this program it won't make any functional difference, but it's good practice.


### The `wc` Child

The `wc` **child** is the reading process. If doesn't use the *write* end of the pipe, so it should close it (**D**). Failing to do so would mean that the `read()` loop in `wc` will never get an `EOF`, and thus would hang.
