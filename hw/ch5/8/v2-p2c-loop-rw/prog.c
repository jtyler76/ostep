#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
    pid_t pid;
    int pipe_fds[2];

    pipe(pipe_fds);

    pid = fork();

    switch ( pid ) {
        case 0:   /* child */
            {
                ssize_t rsize = 0;
                char read_char;

                /* (A) - close the write fd since we aren't using it */
                close(pipe_fds[1]);

                while ( true ) {
                    rsize = read(pipe_fds[0], &read_char, 1);
                    if ( rsize == 0 ) break;
                    printf("%c", read_char);
                }
            }
            break;

        default:  /* parent */
            {
                char *message = "teachings!\n";

                /* (C) - close the read descriptor since we aren't using it */
                close(pipe_fds[0]);

                write(pipe_fds[1], message, strlen(message) + 1);

                /* (B) - close the write fd to signal EOF */
                close(pipe_fds[1]);

                wait(NULL);
            }
            break;
    }

    return EXIT_SUCCESS;
}
