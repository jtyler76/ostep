# Parent -> Child ~ Simple `write()` ~ Looping `read()`

This example creates a pipe with `pipe()`, and then calls `fork()` to create a child process.

The **parent** process writes a string to the write end of the pipe, and then waits for the **child** process to terminate with a call to `wait(NULL)`.

The **child** process performs `read()`'s of one byte at a time in a loop, printing each with `printf()` as they come in. Once `read()` returns `0` to indicate `EOF` the loop breaks and the process terminates.


## Closing Unused Pipe File Descriptors

### Write Descriptors

Since the **child** process continuously reads data from the pipe until it gets an `EOF`, we need to ensure that that actually happens. In order for a `read()` to a pipe to return `EOF`, there must be no data left to read in the pipe, and all `write` file descriptors to the pipe need to be closed.

It is essential that the **child** process close it's own copy of the `write` descriptor to the pipe that it inherited from the parent (**A**), and it is important that the **parent** process close it's write descriptor to the pipe once it's done writing (**B**). Performing (**A**) and (**B**) ensures that the write descriptors for both processes are close as soon as they are no longer needed by each process, and thus ensures that the `read()` to the empty pipe will return `0` indicating `EOF`.

Neglecting to close (**A**) or (**B**) will cause the program to hang.

### Read Descriptors

The reason why you'd want the writing process (in this case the **parent**) to close it's unused file descriptor is because attempts to write to a pipe that has no open read descriptors will cause a `SIGPIPE` signal to the writing process, which will by default terminate that process. Leaving open unnecessary read descriptors prevents this from occuring.

In this particular program it doesn't really matter because the reading process only ever terminates after it sees `EOF` (i.e. all of the write descriptors are closed), but that isn't guaranteed to be the case for all programs. It's good practice to close the unused descriptors all the time, hence (**C**).

Neglecting to close (**C**) will not make a functional difference.
