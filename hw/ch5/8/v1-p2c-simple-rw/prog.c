#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
    pid_t pid;
    int pipe_fds[2];

    pipe(pipe_fds);

    pid = fork();

    switch ( pid ) {
        case 0:   /* child */
            {
                char read_buffer[64];

                read(pipe_fds[0], read_buffer, sizeof(read_buffer));

                printf("%s", read_buffer);
            }
            break;

        default:  /* parent */
            {
                char *message = "teachings!\n";

                write(pipe_fds[1], message, strlen(message) + 1);

                wait(NULL);
            }
            break;
    }

    return EXIT_SUCCESS;
}
