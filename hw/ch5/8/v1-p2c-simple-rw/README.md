# Parent -> Child ~ Simple `read()`/`write()`

This example creates a pipe with `pipe()`, and then calls `fork()` to create a child process.

The **parent** process writes a string to the write end of the pipe, and then waits for the **child** process to terminate with a call to `wait(NULL)`.

The **child** process performs a single `read()` from the read end of the pipe, and printing the content with `printf()`.


## Closing Unused Pipe File Descriptors

It's generally ideal to close the unused file descriptors to a pipe for each process. In this case that would mean that the **parent** would close the `read` end of the pipe, and the **child** would close the *write* end of the pipe.

This doesn't appear to be necessary for this program. I think this is because the **child**'s reading from the pipe is only performed once and doesn't rely on detecting an `EOF`.
