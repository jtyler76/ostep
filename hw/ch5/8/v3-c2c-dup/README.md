# Child -> Child ~ Pipe Connections with `dup()`

This example creates a pipe with `pipe()`, and then `fork()`'s two children. One that `exec()`'s to `ls`, and the other that `exec()`'s to `wc`. The `stdout` of the `ls` process is connected to the write end of the pipe, and the `stdin` of the `wc` process is connected to the read end of the pipe.


## Connecting `stdin`/`stdout` to Pipes Using `dup()`

The `stdin` and `stdout` "files" are described by the `STDIN_FILENO` and `STDOUT_FILENO` fds respectively. The standard library (`libc`) APIs use those fds to write to or read from `stdin` and `stdout`. The default `stdin` and `stdout` (i.e. what `STDIN_FILENO` and `STDOUT_FILENO` point to) should be set up by the shell that you execute the program from.

In order to change the `stdin` and/or `stdout` for a process to the read and/or write ends of a pipe, we need `STDOUT_FILENO` to point to the write end of the pipe, and `STDIN_FILENO` to point to the read end of the pipe. The original way of doing this was with the `dup()` function.

```c
close(STDIN_FILENO);  // close the stdin fd (0)
dup(pipe_fds[0]);     // create a new fd (0) that points to the read end of the pipe
close(pipe_fds[0]);   // close the original fd for the read end of the pipe

// or

close(STDOUT_FILENO);  // close the stdout fd (1)
dup(pipe_fds[1]);      // create a new fd (1) that points to the write end of the pipe
close(pipe_fds[1]);    // close the original fd for the write end of the pipe
```

This works because `dup()` will create a new fd with the lowest available value. Since `stdin`, `stdout` and `stderr` have the lowest possible values (0, 1, 2), once they are closed and followed by a `dup()`, the `dup()` should create the new fd with the now vacant low value.

At this point you'll have 2 fds pointing to the end of the pipe, the original can/should be closed.


## Closing Unused Pipe FDs

### The Parent

The **parent** process doesn't use the pipe at all, so as soon as it has `fork()`'d it's children it should close both ends of the pipe (**A** and **B**).

If the *write* end of the pipe isn't closed (**B**), then the reading process will never get an `EOF` because that only occurs when all FDs to the *write* end of the pipe are closed. This means that the `wc` process would never terminate, continually waiting for the `EOF`.

If the *read* end of the pipe isn't closed (**A**), there will be no functional difference for this program. It's still best practice though. Just know that keeping extra open FDs to the *read* end of the pipe can interfere with the signaling mechanism intended to for the reading process to exit when there are no *read* connections to the pipe.


### The `ls` Child

The `ls` **child** is the writing process. It doesn't use the *read* end of the pipe, so it should close it (**C**). In this program it won't make any functional difference, but it's good practice.


### The `wc` Child

The `wc` **child** is the reading process. If doesn't use the *write* end of the pipe, so it should close it (**D**). Failing to do so would mean that the `read()` loop in `wc` will never get an `EOF`, and thus would hang.
