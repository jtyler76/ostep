#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(void)
{
    pid_t pid = fork();

    if ( pid == 0 ) {
        /* child */
        close(STDOUT_FILENO);
        printf("Hello World!\n");
    } else {
        /* parent */
        wait(NULL);
    }

    return EXIT_SUCCESS;
}
